# Lambda HTTP endpoint

## Assignment

Deploy a production ready function on AWS lambda.
Consider operations management process.

### Added Assumptions
- the user knows how to access the AWS environment on a command line
- the service is deployed in the AWS Sydney region only
- traffic to the service is small (few requests a day) but require low latency

## Solution

I've been using the serverless framework mentioned in the task.

serverless makes the task extremely trivial as it handles the deployment of:
- the handler js code into a lambda function
- the api gateway in charge of the HTTP endpoint, relaying the GET request to our lambda
- the setup and attachment of an IAM role/policies
- (few others resources we don't do mention here)

The task requires the service to be production ready, mentions scalabilty and operational management by another team.

### Load, dimensions & cost

Being production ready means being ready to handle some load and think of potential cost.

Having a really lightweight service allows us to shrink down the MemorySize to the minimum of 128M and save on cost.

To ensure low latency, lambda has a Provisioned Concurrency feature which allows to keep our service hot and running to skip the load time, I've set it to 1 in my template sample, depending on the production requirements we would most likely adjust that number.
This feature comes to a cost to weight in the service cost calculation: see [Lambda Pricing](https://aws.amazon.com/lambda/pricing/)

### Security

Having restricted the serverless configuration to the GET verb without possible parameters, the endpoint is automatically pretty and safe from numerous attacks.
The next level security would be at the lambda permissions which are down to the bare minimum of creating/writing logs for the simple handler sample we're using.

### URL name

With current configuration the deployed endpoint is a `*.amazonaws.com/prod/hello` URL.

In a production environment we would potentially use a custom domain.
We'd require to provide a domain name for which we have authority and an associated AWS ACM certificate to allow serverless to link it all using Route53.

### Monitoring

AWS Lambda logs multiple metrics available via Cloudwatch
- ConcurrentExecutions
- Invocations
- Duration
- Throttles
- Errors

Given the right permissions the operations team would be able to access the above metrics for support.

### Permissions

In order to give access to the operations team for monitoring, they'll need to be given the read-only Actions over the corresponding lambda logs.

The following IAM policy would need to be attached to the operations IAM role using the company preferred way.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:GetLogEvents",
                "logs:DescribeLogStreams"
            ],
            "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/hello-prod*"
        }
    ]
}
```




# Usage

## Pre-requisite

- AWS access
    - Have acess to an account with enough permissions (assumptions say you're admin :thumb-up:)
    - Have your access credentials ready in your ~/.aws/credentials or loaded in your session environment.

- Serverless client
    Have a serverless client installed (version > 2).

    ```
    brew install serverless
    ```
    (a npm version exist)


## Deploy

### Development
By default serverless deploys a `dev` version
```
sls deploy
```

### Production
```
sls deploy --stage production
```



## Monitor

Monitoring of the above mentioned metrics is done via [CloudWatch](https://ap-southeast-2.console.aws.amazon.com/cloudwatch/home?region=ap-southeast-2)

A serverless command line allows to watch the logs in real time:
```
serverless logs -f hello -t
```
